from . import Logger

class BotState:
    def __init__(self, currentRound):
        self.roundId = currentRound["id"]
        self.roundNumber = currentRound["roundNumber"]
        self.ongoingRound = currentRound.get("winnerName") is None

        if self.ongoingRound:
            self.currentHost = currentRound["hostName"]
        else:
            self.roundNumber += 1
            self.currentHost = currentRound["winnerName"]

        Logger.info("State initialized", {
            "roundNumber": self.roundNumber,
            "url": "https://redd.it/" + self.roundId,
            "ongoing": self.ongoingRound,
        })
