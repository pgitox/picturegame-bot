import collections
import json
import math
import requests
import time

from ..utils.Retry import retry

from . import Logger

ROUND_CACHE_MAX_LENGTH = 5

class ApiConnector:
    def __init__(self, config, version):
        self.url = config["picturegameApiUrl"]
        self.username = config["picturegameApiUsername"]
        self.password = config["picturegameApiPassword"]

        self.reportStartup(version)

        self.idToRoundLookup = collections.OrderedDict()

    @retry
    def request(self, endpoint, method, rawData = None):
        data = None
        headers = None

        if rawData is not None:
            data = json.dumps(rawData)
            headers = { "Content-Type": "application/json" }

        Logger.debug("Sending request to API", { "method": method.__name__, "endpoint": endpoint })
        request = method(self.url + endpoint, data = data, headers = headers, auth = (self.username, self.password))

        if request.status_code // 100 != 2:
            # TODO: retry or handle case by case
            Logger.error("Unexpected HTTP status code", {
                "method": method.__name__,
                "endpoint": endpoint,
                "status": request.status_code,
            })
            return None

        return json.loads(request.text)

    def getCurrent(self):
        return self.request("/current", requests.get)["round"]

    def getRoundById(self, submissionId, refresh = False):
        if not refresh and submissionId in self.idToRoundLookup:
            Logger.debug("Using cached round data", { "submissionId": submissionId })
            return self.idToRoundLookup[submissionId]

        endpoint = "/rounds?filter=id eq '{}'&select=winningCommentId,winnerName,hostName".format(submissionId)
        response = self.request(endpoint, requests.get)
        rounds = response['results']
        roundData = rounds[0] if rounds else None

        self.cacheRound(submissionId, roundData)
        return roundData

    def cacheRound(self, submissionId, roundData):
        while len(self.idToRoundLookup) >= ROUND_CACHE_MAX_LENGTH:
            self.idToRoundLookup.popitem(last = False)

        self.idToRoundLookup[submissionId] = roundData

    def postRound(self, roundNumber, submission):
        roundData = {
            "roundNumber": roundNumber,
            "hostName": submission.author.name,
            "id": submission.id,
            "postTime": submission.created_utc,
            "postUrl": submission.url,
            "thumbnailUrl": submission.thumbnail,
            "title": submission.title,
        }

        self.request("/rounds", requests.post, roundData)

    def patchRound(self, roundNumber, winningComment, plusCorrectComment):
        endpoint = "/rounds/{}".format(roundNumber)
        roundData = {
            "winningCommentId": winningComment.id,
            "winTime": winningComment.created_utc,
            "winnerName": winningComment.author.name,
            "plusCorrectTime": plusCorrectComment.created_utc,
        }

        return self.request(endpoint, requests.patch, roundData)

    def deleteRound(self, roundNumber):
        endpoint = "/rounds/{}".format(roundNumber)
        self.request(endpoint, requests.delete)

    def reportStartup(self, version):
        data = {
            "name": "picturegame-bot",
            "version": version,
            "startTime": math.floor(time.time()),
        }
        self.request("/status", requests.post, data)
