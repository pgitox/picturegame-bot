from datetime import datetime
import traceback
import sys

from ..const import APPROVAL_REQ_REPLY
from ..reddit import utils as RedditUtils

from . import Logger

ISO_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S.%f%z'

class ModmailPoller:
    def __init__(self, subreddit, botName):
        self.modmail = subreddit.modmail
        self.subredditName = subreddit.display_name
        self.botName = botName
        self.expectingNotification = False
        self.messagesMarkedRead = []

    def clearNotification(self):
        '''Force the poller to look at notifications on the next run, even if there are no unreads.
        Works around the fact that the bot's own notifications are automatically marked as read.
        '''
        self.expectingNotification = True

    def checkModmail(self):
        messages = []

        for cat in self._unreadCategories():
            for message in self._readMessages(cat):
                messages.append(message)

        # Clear this here in case we didn't find a notification when we looked
        self.expectingNotification = False

        self._markRead()

        return sorted(messages, key = lambda m: m['date'])

    def _unreadCategories(self):
        rawCounts = None
        try:
            rawCounts = self.modmail.unread_count()
        except:
            onError('Failed to check modmail unread counts')
            return

        # Don't care about the categories or how many there are
        # Just need to know which out of "all" and "mod" we need to fetch
        # Note that we don't include highlighted here as they fall into the other categories too

        if rawCounts['mod'] > 0:
            yield 'mod'

        if rawCounts['join_requests'] > 0:
            yield 'join_requests'

        if self.expectingNotification or \
            rawCounts['notifications'] > 0 or \
            rawCounts['new'] > 0 or \
            rawCounts['inprogress'] > 0 or \
            rawCounts['appeals'] > 0:
            yield 'all'

    def _readMessages(self, convType):
        conversations = None
        try:
            conversations = self.modmail.conversations(sort = 'unread', state = convType)
        except:
            onError('Failed to fetch modmail conversations')
            return

        for conv in conversations:
            if convType == 'join_requests':
                RedditUtils.reply(conv, APPROVAL_REQ_REPLY, self.subredditName, username = conv.participant.name)
                archive(conv)
                continue

            if isApprovedSubmitter(conv, self.botName):
                archive(conv)
                self.expectingNotification = False
                continue

            if conv.last_unread is None:
                # Reached a conversation that's been read, no more unreads to see
                if self.expectingNotification and convType != 'mod':
                    # If we're expecting a notification, we need to keep looking
                    continue
                else:
                    break

            for message in self._getMessagesFromConv(conv):
                yield message

            self.messagesMarkedRead.append(conv)

    def _getMessagesFromConv(self, conv):
        unreadTime = datetime.strptime(conv.last_unread, ISO_DATE_FORMAT)

        # Iterate through the messages backwards until we get to one we've already seen
        for message in conv.messages[::-1]:
            if message.author.name.lower() == self.botName:
                continue

            messageTime = datetime.strptime(message.date, ISO_DATE_FORMAT)

            yield {
                'conversation': conv.id,
                'subject': conv.subject,
                'author': message.author.name,
                'body': message.body_markdown,
                'date': messageTime,
            }

            # Note that we actually did yield this one even though it was posted before the unreadTime.
            # The Reddit API seems to set the unread time a few milliseconds before the post time of the message.
            # This is undocumented behavior so this is a bit of a hack.
            if messageTime < unreadTime:
                break

    def _markRead(self):
        if self.messagesMarkedRead:
            try:
                # Horrible PRAW API: Read one conversation and also read all the other ones
                self.messagesMarkedRead[0].read(self.messagesMarkedRead[1:])
                self.messagesMarkedRead = []
            except:
                onError('Failed to mark modmails as read')


def isApprovedSubmitter(conv, botName):
    return 'approved' in conv.subject.lower() and \
        conv.num_messages == 1 and \
        conv.authors[0].name.lower() == botName

def archive(conv):
    try:
        # Note: this also marks the conversation as read
        conv.archive()
    except:
        onError('Failed to archive modmail conversation')

def onError(message):
    etype, evalue, _tb = sys.exc_info()
    # Modmail operations are not critical to the game, and this should only fail due to errors on Reddit's end.
    # therefore don't log this as an error
    Logger.warn(message, { 'error': traceback.format_exception_only(etype, evalue) })
