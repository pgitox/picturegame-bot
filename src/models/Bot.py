from time import sleep

import praw

from ..reddit import Comment
from ..reddit import Post
from ..reddit import User
from ..reddit import utils as RedditUtils

from ..utils.MarkdownUtils import escapeChars
from ..utils.Retry import retry
from ..utils import RoundUtils

from ..const import DUPLICATE_ROUND_REPLY, NEW_ROUND_COMMENT, \
    OVER_FLAIR, PLUSCORRECT_REPLY, UNSOLVED_FLAIR, WINNER_PM, WINNER_SUBJECT, \
    CORRECT_NEEDS_VERIFICATION_HOST, CORRECT_NEEDS_VERIFICATION_MOD, \
    WIN_TRANSFER_LATEST, WIN_TRANSFER_PAST, \
    CORRECT_PATTERN

from .ApiConnector import ApiConnector
from .BotState import BotState
from .Mail import ModmailPoller
from .RedditPoller import RedditPoller
from . import Logger

class Bot:
    def __init__(self, config, version, discord):
        self.config = config

        self.api = ApiConnector(config, version)
        self.discord = discord
        self.reddit = praw.Reddit(config["scriptName"])
        self.subreddit = self.reddit.subreddit(config["subredditName"])
        self.subredditName = self.subreddit.display_name
        self.name = self.getBotName()

        self.submissions = RedditPoller(self.subreddit.new)
        self.comments = RedditPoller(self.subreddit.comments)
        self.modmail = ModmailPoller(self.subreddit, self.name)

        currentRound = self.api.getCurrent()
        self.state = BotState(currentRound)

        self.mods = RedditUtils.getMods(self.subreddit)

        self.correctBlacklist = set([s.lower() for s in config["correctBlacklist"].split(',')])

    @retry
    def getBotName(self):
        return self.reddit.user.me().name.lower()

    def mainLoop(self):
        interval = self.config.getint("redditPollInterval")
        while True:
            self.checkDeletedOrAbandoned()

            for submission in self.submissions.getLatest():
                if submission is None:
                    break
                if not RedditUtils.isDeletedOrRemoved(submission):
                    self.onSubmission(submission)

            for comment in self.comments.getLatest():
                if comment is None:
                    break
                if not RedditUtils.isDeletedOrRemoved(comment):
                    self.onComment(comment)

            for message in self.modmail.checkModmail():
                self.onModmail(message)

            sleep(interval)

    @retry
    def checkDeletedOrAbandoned(self):
        if not self.state.ongoingRound:
            return

        currentRound = self.reddit.submission(id = self.state.roundId)
        if Post.checkDeleted(currentRound):
            self.state.ongoingRound = False
            self.api.deleteRound(self.state.roundNumber)
            self.discord.reportRoundStatus({ "status": "deleted" })
            return

        abandonmentReason = Post.checkAbandoned(currentRound, self.subreddit, self.state.currentHost)
        if abandonmentReason is not None:
            self.state.ongoingRound = False
            self.state.roundNumber += 1

            self.discord.reportRoundStatus({ "status": abandonmentReason })

    def onSubmission(self, submission):
        if submission.is_self:
            # assume that non-mods cannot post self-posts
            self.discord.reportModPost(submission)
            return
        if submission.id <= self.state.roundId:
            return

        Logger.debug("Got new submission", { "id": submission.id, "author": submission.author.name })

        if not self.state.ongoingRound:
            if Post.validate(submission):
                self.onPotentialNewRound(submission)
            else:
                Logger.debug("Submission is not a valid round", { "id": submission.id })
        else:
            # New submission while a round is ongoing
            RedditUtils.commentReply(submission,
                DUPLICATE_ROUND_REPLY,
                self.subredditName,
                roundId = self.state.roundId)
            RedditUtils.removeEntity(submission)

    def onComment(self, comment):
        Logger.debug("Got new comment", {
            "id": comment.id,
            "submissionId": comment.submission.id,
            "author": comment.author.name,
        })

        if comment.author.name.lower() != self.name:
            self.discord.reportNewComment(comment)

        parent = comment.parent()

        if comment.submission.id == self.state.roundId and self.state.ongoingRound:
            if Comment.isCurrentRoundCorrect(comment, parent, self.name, self.mods, self.correctBlacklist):
                self.onRoundOver(comment, parent)

        else:
            if Comment.isPastRoundCorrect(comment, parent, self.name, self.mods, self.api, self.correctBlacklist, refreshRoundData = True):
                self.onPastRoundPlusCorrect(comment, parent)

    def onPotentialNewRound(self, submission):
        if Post.rejectIfInvalid(submission, self.state.roundNumber, self.subredditName):
            self.onNewRound(submission)

    @retry
    def onNewRound(self, submission):
        Logger.debug("Submission is a new round", { "id": submission.id })
        self.mods = RedditUtils.getMods(self.subreddit)

        self.api.postRound(self.state.roundNumber, submission)

        Post.setFlair(submission, UNSOLVED_FLAIR)

        postAuthor = RedditUtils.getPostAuthorName(submission)
        if postAuthor != self.state.currentHost:
            # de-perm the expected host if someone else took over
            RedditUtils.removeContributor(self.subreddit, self.state.currentHost)

        RedditUtils.commentReply(submission,
            NEW_ROUND_COMMENT,
            self.subredditName,
            hostName = escapeChars(postAuthor))

        self.state.ongoingRound = True
        self.state.roundId = submission.id
        self.state.currentHost = postAuthor

        self.discord.reportRoundStatus({
            "status": "new",
            "url": submission.url,
            "id": submission.id,
            "host": postAuthor,
            "title": submission.title,
            "roundNumber": self.state.roundNumber,
            "postTime": submission.created_utc,
        })

        Logger.info("New round active", {
            "roundNumber": self.state.roundNumber,
            "id": submission.id,
            "host": postAuthor,
        })

    @retry
    def onRoundOver(self, comment, winningComment):
        roundWinner = winningComment.author
        Logger.info("Round won", {
            "roundNumber": self.state.roundNumber,
            "winner": roundWinner.name,
            "commentId": winningComment.id,
        })

        roundNumber = self.state.roundNumber

        newWinner = self.api.patchRound(roundNumber, winningComment, comment)["newWinner"]
        rounds = newWinner["roundList"]

        RedditUtils.commentReply(winningComment, PLUSCORRECT_REPLY, self.subredditName)

        RedditUtils.removeContributor(self.subreddit, self.state.currentHost)
        RedditUtils.addContributor(self.subreddit, roundWinner.name)
        self.modmail.clearNotification() # Clear the modmail notification from adding the contributor

        RedditUtils.sendMessage(roundWinner, WINNER_SUBJECT, WINNER_PM,
            self.subredditName,
            roundNum = roundNumber + 1)

        self.state.roundNumber += 1
        self.state.currentHost = roundWinner.name
        self.state.ongoingRound = False

        User.setFlair(self.subreddit, rounds, winningComment, len(rounds) - 1)
        Post.setFlair(winningComment.submission, OVER_FLAIR)

        self.discord.reportRoundStatus({
            "status": "solved",
            "winner": roundWinner.name,
            "winCount": len(rounds),
            "streak": RoundUtils.countWinStreak(rounds),
            "gap": RoundUtils.roundsSinceLastWin(rounds),
            "commentId": winningComment.id,
            "winTime": winningComment.created_utc,
        })

        existingSticky = Comment.getStickyComment(winningComment.submission)
        Comment.postSticky(winningComment, self.subredditName, existingSticky)

        Logger.debug("Post-round cleanup done")

    @retry
    def onPastRoundPlusCorrect(self, comment, parent):
        Logger.info("Found a +correct on a past round", {
            "comment": comment.id,
            "parentComment": parent.id,
            "submission": comment.submission.id,
            "author": comment.author.name,
            "targetWinner": parent.author.name,
        })

        # One reddit request; this should in theory fetch all top-level replies to the target comment
        parent.refresh()

        isSecondCorrect = False
        botReply = None

        for sibling in parent.replies:
            if sibling.author is None or sibling.author.name == comment.author.name:
                # The same person can't verify. Note this also skips the comment that brought us here
                continue

            if sibling.author.name.lower() == self.name:
                botReply = sibling

            isSecondCorrect = isSecondCorrect or \
                Comment.isPastRoundCorrect(sibling, parent, self.name, self.mods, self.api, self.correctBlacklist)

            if isSecondCorrect and botReply is not None:
                break

        if isSecondCorrect:
            self.onSecondCorrect(comment, parent, botReply)
        elif botReply is None:
            self.onFirstCorrect(comment, parent)
        else:
            # If the bot has already replied then this was a second +correct by the same person, so ignore it
            Logger.debug("+correct comment ignored; it was probably written by the same person as the first one")

    @retry
    def onFirstCorrect(self, comment, parent):
        commentBody = CORRECT_NEEDS_VERIFICATION_HOST if comment.is_submitter else CORRECT_NEEDS_VERIFICATION_MOD
        RedditUtils.commentReply(parent, commentBody, self.subredditName, correcter = comment.author.name)

        roundNumber = self.api.getRoundById(comment.submission.id)['roundNumber']
        self.discord.reportRoundCorrection(comment, parent, roundNumber)

    @retry
    def onSecondCorrect(self, comment, parent, botReply):
        if botReply is not None:
            RedditUtils.deleteComment(botReply)

        roundData = self.api.getRoundById(comment.submission.id)
        roundNumber = roundData["roundNumber"]

        prevWinnerId = roundData.get('winningCommentId')
        prevWinningComment = None
        if prevWinnerId:
            prevWinningComment = praw.models.Comment(self.reddit, id = prevWinnerId)
            prevWinningComment.refresh()
            for child in prevWinningComment.replies:
                if CORRECT_PATTERN.search(child.body):
                    RedditUtils.removeEntity(child)
                    continue

                if child.author is not None and child.author.name.lower() == self.name:
                    RedditUtils.deleteComment(child)
                    continue

        isCurrentRound = comment.submission.id == self.state.roundId
        previousWinner = roundData.get('winnerName')

        replyTemplate = WIN_TRANSFER_LATEST if isCurrentRound else WIN_TRANSFER_PAST
        RedditUtils.commentReply(parent, replyTemplate, self.subredditName, previousWinner = previousWinner)

        if isCurrentRound:
            if previousWinner != parent.author.name:
                if previousWinner is not None:
                    RedditUtils.removeContributor(self.subreddit, previousWinner)

                # Update the expected host, so that they'll get de-permed if someone else posts
                self.state.currentHost = parent.author.name

                RedditUtils.addContributor(self.subreddit, parent.author)
                self.modmail.clearNotification() # Clear the modmail notification from adding the contributor

            RedditUtils.sendMessage(parent.author, WINNER_SUBJECT, WINNER_PM,
                self.subredditName,
                roundNum = roundNumber + 1)

        if not prevWinnerId:
            # If there wasn't already a winning comment, it must have been abandoned
            Post.setFlair(parent.submission, OVER_FLAIR)

        response = self.api.patchRound(roundNumber, parent, comment)

        if prevWinningComment is not None and "oldWinner" in response:
            prevWinnerRounds = response["oldWinner"]["roundList"]
            User.setFlair(self.subreddit, prevWinnerRounds, prevWinningComment, len(prevWinnerRounds) + 1)

        if "newWinner" in response:
            # If the winner didn't change, then the API doesn't include it in the response
            # But this means we don't need to update the flair anyway!
            newWinnerRounds = response["newWinner"]["roundList"]
            User.setFlair(self.subreddit, newWinnerRounds, parent, len(newWinnerRounds) - 1)

        sticky = Comment.getStickyComment(comment.submission)
        if sticky is not None and sticky.author is not None and sticky.author.name.lower() == self.name:
            # TODO: If we've already posted one, we could edit it (1 request) instead of deleting/reposting (3 requests)
            RedditUtils.deleteComment(sticky)
            sticky = None

        Comment.postSticky(parent, self.subredditName, sticky)

        self.discord.reportCompletedRoundCorrection(parent, roundNumber)

    def onModmail(self, message):
        self.discord.reportModMail(message)
