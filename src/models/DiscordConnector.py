import json
import socket

from . import Logger

class DiscordConnector:
    def __init__(self, config):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.addr = config.get("discordAddress", "localhost")
        self.port = int(config.get("discordPort", 12345))

    def send(self, message):
        bytesSent = self.socket.sendto(bytes(json.dumps(message), "utf-8"), (self.addr, self.port))
        Logger.debug("Sent message to Discord bot", {
            "bytes": bytesSent,
            "addr": self.addr,
            "port": self.port,
        }, sendToDiscord = False)

    def reportRoundStatus(self, data):
        Logger.info("Sending communication to discord for round status", { "status": data["status"] }, sendToDiscord = False)
        data["type"] = "current-round"
        self.send(data)

    def reportNewComment(self, comment):
        Logger.debug("Sending communication to discord for comment", { "id": comment.id }, sendToDiscord = False)
        self.send({
            "type": "comment",
            "author": comment.author.name,
            "isOp": comment.is_submitter,
            "postId": comment.submission.id,
            "commentId": comment.id,
            "parentId": comment.parent_id,
            "body": comment.body,
        })

    def reportModPost(self, submission):
        Logger.info("Sending communication to discord for mod post", { "id": submission.id }, sendToDiscord = False)
        self.send({
            "type": "mod-post",
            "author": submission.author.name,
            "title": submission.title,
            "postId": submission.id,
        })

    def reportModMail(self, message):
        Logger.info("Sending communication to discord for modmail message", { "conversation": message["conversation"] }, sendToDiscord = False)
        self.send({
            "type": "mod-mail",
            "conversation": message["conversation"],
            "subject": message["subject"],
            "author": message["author"],
            "body": message["body"],
        })

    def reportRoundCorrection(self, comment, targetComment, roundNumber):
        Logger.info("Sending communication to discord for round correction", {
            "submission": targetComment.submission.id,
            "comment": targetComment.id,
            "roundNumber": roundNumber,
        }, sendToDiscord = False)
        self.send({
            "type": "round-correction",
            "ordinal": 0,
            "commentId": targetComment.id,
            "submissionId": targetComment.submission.id,
            "correcter": comment.author.name,
            "winner": targetComment.author.name,
            "roundNumber": roundNumber,
        })

    def reportCompletedRoundCorrection(self, comment, roundNumber):
        Logger.info("Sending communication to discord for completed round correction", {
            "submission": comment.submission.id,
            "comment": comment.id,
            "roundNumber": roundNumber,
        }, sendToDiscord = False)
        self.send({
            "type": "round-correction",
            "ordinal": 1,
            "commentId": comment.id,
            "submissionId": comment.submission.id,
            "winner": comment.author.name,
            "winTime": comment.created_utc,
            "roundNumber": roundNumber,
        })

    def reportLogMessage(self, level, message, data):
        Logger.debug("Sending communication to discord for log message", { "level": level }, sendToDiscord = False)
        self.send({
            "type": "log",
            "level": level,
            "message": message,
            "data": data
        })
