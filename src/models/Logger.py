import datetime
import json
import logging
import logging.config
import sys
from threading import Timer, Lock

try:
    from elasticsearch import helpers as eshelpers
    from elasticsearch import Elasticsearch, RequestsHttpConnection
    from elasticsearch.serializer import JSONSerializer
    ES_SUPPORTED = True
except ImportError:
    ES_SUPPORTED = False

ES_DEFAULT_LOG_BUFFER_SIZE = 512
ES_DEFAULT_LOG_FLUSH_DELAY_S = 4
ES_DEFAULT_HOST = "127.0.0.1"
ES_DEFAULT_PORT = 9200

class EsLogger:
    def __init__(self, appConfig):
        if not ES_SUPPORTED:
            raise Exception("elasticsearch library required if ES logging is enabled.")

        self.indexName = appConfig.get("esIndexName")
        if self.indexName is None:
            raise Exception("esIndexName must be provided if ES logging is enabled.")

        self._timer = None
        self._buffer = []
        self._bufferLock = Lock()

        self._bufferSize = appConfig.get("esBufferSize", ES_DEFAULT_LOG_BUFFER_SIZE)
        self._flushDelayS = appConfig.get("esFlushDelaySeconds", ES_DEFAULT_LOG_FLUSH_DELAY_S)

        self._client = Elasticsearch(
            hosts=[{
                "host": appConfig.get("esHost", ES_DEFAULT_HOST),
                "port": appConfig.get("esPort", ES_DEFAULT_PORT),
            }],
            use_ssl = False,
            verify_certs = False,
            connection_class = RequestsHttpConnection,
            serializer = JSONSerializer())

    def log(self, level, message, metadata):
        record = dict(metadata) if metadata is not None else {}

        date = datetime.datetime.utcnow()
        record["timestamp"] = "{0!s}.{1:03d}Z".format(
            date.strftime('%Y-%m-%dT%H:%M:%S'),
            int(date.microsecond / 1000))

        record["level"] = logging.getLevelName(level)
        record["message"] = message

        record = {
            "_index": self._getIndexName(),
            "_type": "python_log",
            "_source": record,
        }

        with self._bufferLock:
            self._buffer.append(record)

        if len(self._buffer) >= self._bufferSize:
            self._flush()
        else:
            self._asyncFlush()

    def close(self):
        if self._timer is not None:
            self._flush()
        self._timer = None

    def _getIndexName(self):
        return "{0!s}-{1!s}".format(self.indexName, datetime.datetime.now().strftime('%Y.%m'))

    def _asyncFlush(self):
        if self._timer is None:
            self._timer = Timer(self._flushDelayS, self._flush)
            self._timer.setDaemon(True)
            self._timer.start()

    def _flush(self):
        if self._timer is not None and self._timer.is_alive():
            self._timer.cancel()
        self._timer = None

        if self._buffer:
            try:
                with self._bufferLock:
                    actions = self._buffer
                    self._buffer = []

                eshelpers.bulk(client = self._client, actions = actions, stats_only = True)
            except Exception as e:
                sys.stderr.write(f"Error writing {len(actions)} records to ES\n{e}\n")

class Logger:
    __instance = None

    @staticmethod
    def getInstance():
        return Logger.__instance

    def __init__(self, logConfig, appConfig, discord):
        logging.config.dictConfig(logConfig)
        self.logger = logging.getLogger('picturegame-bot')

        self.esLogger = None
        enableEsLogging = int(appConfig.get("enableEsLogging", 0))
        if enableEsLogging:
            self.esLogger = EsLogger(appConfig)

        self.discord = discord

        Logger.__instance = self
        info("Successfully configured logging")

    def log(self, message, metadata, level, sendToDiscord):
        if metadata is None:
            self.logger.log(level, message)
        else:
            self.logger.log(level, '%s %s', message, metadata)

        if self.esLogger is not None:
            self.esLogger.log(level, message, metadata)

        if sendToDiscord:
            self.discord.reportLogMessage(logging.getLevelName(level).lower(), message, metadata)


def setupLogging(logConfigFileName, config, discord):
    logConfig = None
    with open(logConfigFileName) as logConfigFile:
        logConfig = json.loads(logConfigFile.read())

    Logger(logConfig, config, discord)

def debug(message, metadata = None, sendToDiscord = True):
    Logger.getInstance().log(message, metadata, logging.DEBUG, sendToDiscord)

def info(message, metadata = None, sendToDiscord = True):
    Logger.getInstance().log(message, metadata, logging.INFO, sendToDiscord)

def warn(message, metadata = None, sendToDiscord = True):
    Logger.getInstance().log(message, metadata, logging.WARNING, sendToDiscord)

def error(message, metadata = None, sendToDiscord = True):
    Logger.getInstance().log(message, metadata, logging.ERROR, sendToDiscord)

def fatal(message, metadata=None, sendToDiscord = True):
    Logger.getInstance().log(message, metadata, logging.FATAL, sendToDiscord)

def flush():
    logger = Logger.getInstance()
    if logger.esLogger is not None:
        logger.esLogger.close()
