# pylint: disable=line-too-long

import re

TITLE_PATTERN = re.compile(r"^\[round \d+\]", re.I) # ignore case
TITLE_CORRECTION_PATTERN = re.compile(
    r"^\s*[\(\{\[\<]?\s*(round)?\s*\d*\s*[\)\}\]\>]?\s*:?\s*", re.I)
CORRECT_PATTERN = re.compile(r"\+correct")
HIGH_FLAIR_PATTERN = re.compile(r"^(\d+ wins)|$")

UNSUPPORTED_SPOILER_MD_PREFIX = re.compile(r"^(\#+)|(\d+\.\s+)|(\*\s+)|(\+\s+)|(>!?\s*)")
UNSUPPORTED_SPOILER_MD_SUFFIX = re.compile(r"!<$")

# --- Coordinates
DECIMAL_OR_DMS = re.compile(
    """(?:((?:[-+]?\d{1,2}[.]\d+),\s*(?:[-+]?\d{1,3}[.]\d+))|(\d{1,3}°\d{1,3}'\d{1,3}\.\d\"[N|S]\s\d{1,3}°\d{1,3}'\d{1,3}\.\d\"[E|W]))"""
)
EVERYTHING_ELSE = re.compile(
    """(^| )(-?\d{1,2}(\.\d+)?(?=\s*,?\s*)[\s,]+-?\d{1,3}(\.\d+)?|\d{1,2}(\.\d+°|°(\d{1,2}(\.\d+'|'(\d{1,2}(\.\d+)?\")?))?)[NS](?=\s*,?\s*)[\s,]+\d{1,3}(\.\d+°|°(\d{1,2}(\.\d+'|'(\d{1,2}(\.\d+)?\")?))?)[EW])"""
)
# Opens a gmaps with pin @coords (decimal) in satellite view
MAPS_URL = 'https://maps.google.com/maps?t=k&q=loc:{coords}'
# ---
def LOW_FLAIR_PATTERN(numRounds):
    pattern = r"^(Round \d+(, \d+){0,%d})|$" % (numRounds - 1)
    return re.compile(pattern)

UNSOLVED_FLAIR = "UNSOLVED"
OVER_FLAIR = "ROUND OVER"
ABANDONED_FLAIRS = {OVER_FLAIR, "ABANDONED", "TERMINATED"}

COMMENT_URL = "/comments/{postId}/_/{commentId}"

CONFIG_FILENAME = "bot.ini"

COMMENT_FOOTER = '''

---
^^I ^^am ^^a ^^bot. ^^If ^^I ^^don't ^^work, ^^please [^^PM ^^my ^^master](https://www.reddit.com/message/compose/?to=Provium)
^^or [^^message ^^the ^^moderators.](https://www.reddit.com/message/compose?to=%2Fr%2F{subredditName})
[^^Learn ^^more](/r/PictureGame_Bot)'''

TIPS_MESSAGE = '''A few tips:

* I've DM'd you a link to submit your round with the correct title.
* Make sure your round can't be solved using Reverse-Image-Search (RIS):
    * Google isn't the only RIS engine! Try using a browser extension like Reveye ([Chrome](https://chrome.google.com/webstore/detail/keaaclcjhehbbapnphnmpiklalfhelgf), [Firefox](https://addons.mozilla.org/en-US/firefox/addon/reveye-ris/)) to check multiple engines at once.
    * If you need to, you can apply a mask to your image to trick RIS. Try our online masking tool [here](https://imagemasker.github.io).
* Once you've got the next round up, let us know how you solved this one!
* Feel free to join us in our [Discord Server](https://discord.gg/D2t9fN2) to discuss your round and future rounds.
* For more detailed information, check out our [Complete Rules](/r/{subredditName}/wiki/rules) and [Hosting Guide](/r/{subredditName}/wiki/hosting).
'''

PLUSCORRECT_REPLY = '''Congratulations, that was the correct answer!
It's your turn to post the next round - please continue the game **in the next 5 minutes**.

''' + TIPS_MESSAGE

WINNER_SUBJECT = "Congratulations, it's your turn to post the next round!"
WINNER_PM = '''
Congratulations on winning the last round!
Your account should now be approved to submit to /r/{subredditName} to submit a new round.

Please remember that your title must start with "[Round {roundNum}]".

---
>[Submit a new Round](https://www.reddit.com/r/{subredditName}/submit?title=[Round%20{roundNum}])

*Please note that the above link may not work in some mobile Reddit apps. If you have trouble, try manually submitting your round to /r/{subredditName}.*
'''

ROUND_OVER_STICKY = '''#Congratulations to {winnerName} on winning this round!

The correct answer was:

{spoileredAnswer}

[Go to winning comment]({commentLink})'''

ROUND_OVER_STICKY_WITH_COORDS = '''#Congratulations to {winnerName} on winning this round!

The correct answer was:

{spoileredAnswer}

[View in Google Maps]({mapsLink})

[Go to winning comment]({commentLink})'''

ROUND_OVER_STICKY_NO_ANSWER = '''#Congratulations to {winnerName} on winning this round!

[Go to winning comment]({commentLink})'''

REJECTION_COMMENT = '''Your submission has been rejected because you have not titled it correctly!

Please re-post your round with the following title:

[{correctTitle}](https://www.reddit.com/r/{subredditName}/submit?title={correctTitle})'''

NEW_ROUND_COMMENT = '''#*{hostName}*:

Thank you for posting a new round.
If a user guesses correctly simply respond with *+correct*; the bot will do the rest of the work for you.
Note that this is **case sensitive** and the bot will respond to it no matter where it is in your comment.

If you need to leave, you must send the answer to [modmail](https://www.reddit.com/message/compose?to=%2Fr%2F{subredditName}),
along with enough information for us to take over hosting your round.

**Confused or new?** See the [hosting guide](/r/{subredditName}/wiki/hosting) for the answer to all your problems.

#*Other users*:

Please remember if you answer correctly you will be expected to **post the next round within 5 minutes of winning**, and **be available to host it for at least an hour.**

**New?** See our [guide](/r/{subredditName}/wiki/beginners) and [rules](/r/{subredditName}/wiki/rules)

#*Chat*

Join the official [{subredditName} Discord](https://discord.gg/D2t9fN2) chat to discuss this and future rounds!'''

DUPLICATE_ROUND_REPLY = '''It looks like you've posted something new while there is already a round ongoing [here](https://redd.it/{roundId}/).

If you want to post a new round, please **delete the earlier posting** first, but be mindful that you should not do this if the current round has been
running for more than a few minutes.'''

CORRECT_NEEDS_VERIFICATION_MOD = '''/u/{correcter} has attempted to award you the win for this comment, but I need confirmation before going ahead.

If either the host or another mod replies +correct to your comment, you will be awarded the win.'''

CORRECT_NEEDS_VERIFICATION_HOST = '''/u/{correcter} has attempted to award you the win for this comment, but I need confirmation before going ahead.

If a mod replies +correct to your comment, you will be awarded the win.'''

WIN_TRANSFER_PAST = '''Congratulations, that was the correct answer!
Since we've already moved on with another round, you won't be able to post the next one.'''

WIN_TRANSFER_LATEST = '''Congratulations, that was the correct answer!
I've approved you to post the next round, instead of /u/{previousWinner}. Please continue the game **in the next 5 minutes**.

''' + TIPS_MESSAGE

APPROVAL_REQ_REPLY = '''Hi {username}, thanks for your interest in PictureGame!

In order to post a new round, you have to win the current one. Please read our [rules](/r/{subredditName}/wiki/rules) before playing.
Once you win a round, you will be automatically approved to post the next one.

For more information and to engage with the PictureGame community, feel free to join our [Discord server](https://discord.gg/D2t9fN2).'''
