from prawcore.exceptions import Forbidden

from ..const import LOW_FLAIR_PATTERN, HIGH_FLAIR_PATTERN

from ..models import Logger
from ..utils.Retry import retry

@retry
def getFlairIdFromCssClass(subreddit, cssClass):
    for flair in subreddit.flair.templates:
        if flair["css_class"] == cssClass:
            return flair["id"]

    return None

@retry
def setFlair(subreddit, rounds, comment, prevNumWins):
    flairData = None

    try:
        flairData = extractFlair(subreddit, comment, prevNumWins)
    except InvalidFlairException:
        # Flair is not in the expected format, leave it alone
        return

    newFlair = generateFlair(rounds) + flairData.get("customFlair", "")
    if not newFlair:
        # Clear the template as well as the text, otherwise the text defaults to the template text
        trySetFlairById(subreddit, comment.author, newFlair, None)
        return

    if flairData["templateId"] is not None:
        if trySetFlairById(subreddit, comment.author, newFlair, flairData["templateId"]):
            return

        # Template might have been deleted
        # If it's been replaced, we can use the new one, otherwise we'll need to fallback to css class
        templateId = getFlairIdFromCssClass(subreddit, flairData["cssClass"])
        if templateId is not None and templateId != flairData["templateId"]:
            if trySetFlairById(subreddit, comment.author, newFlair, templateId):
                return

    Logger.warn("Fallback to setting flair by css class", {
        "cssClass": flairData["cssClass"],
        "templateId": flairData["templateId"],
    })
    subreddit.flair.set(comment.author, text = newFlair, css_class = flairData["cssClass"])

@retry
def trySetFlairById(subreddit, user, text, templateId):
    try:
        subreddit.flair.set(user, text = text, flair_template_id = templateId)
        return True
    except Forbidden:
        Logger.warn("Unable to set flair by templateId", { "templateId": templateId })
        return False

def extractFlair(subreddit, comment, prevNumWins):
    oldFlair = comment.author_flair_text or ""
    data = {}

    if (prevNumWins == 0 and oldFlair != "") or \
        (0 < prevNumWins < 8 and not LOW_FLAIR_PATTERN(prevNumWins).match(oldFlair)) or \
        (prevNumWins >= 8 and not HIGH_FLAIR_PATTERN.match(oldFlair)):

        raise InvalidFlairException

    if 0 < prevNumWins < 8:
        data["customFlair"] = LOW_FLAIR_PATTERN(prevNumWins).sub("", oldFlair)
    elif prevNumWins >= 8:
        data["customFlair"] = HIGH_FLAIR_PATTERN.sub("", oldFlair)

    data["cssClass"] = comment.author_flair_css_class or "winner"
    data["templateId"] = comment.author_flair_template_id or getFlairIdFromCssClass(subreddit, data["cssClass"])

    return data

def generateFlair(rounds):
    numWins = len(rounds)

    if numWins == 0:
        return ""

    if numWins < 8:
        return "Round " + ", ".join([str(roundNum) for roundNum in rounds])

    return "{} wins".format(numWins)


class InvalidFlairException(Exception):
    pass
