import re

from ..const import TITLE_CORRECTION_PATTERN, REJECTION_COMMENT, \
    ABANDONED_FLAIRS, OVER_FLAIR

from ..models import Logger

from ..utils.Retry import retry

from . import utils

flairChoiceCache = None
ROUND_TITLE_PATTERN = re.compile(r"^\[round (\d+)\]", re.I)

@retry
def setFlair(submission, flair):
    if flairChoiceCache is None:
        getFlairChoices(submission)

    flairId = flairChoiceCache[flair]
    submission.flair.select(flairId)


@retry
def getFlairChoices(submission):
    global flairChoiceCache

    flairs = submission.flair.choices()
    flairChoices = {}
    for flair in flairs:
        flairChoices[flair["flair_text"]] = flair["flair_template_id"]

    flairChoiceCache = flairChoices


@retry
def validate(submission):
    '''Check that the post meets the following conditions:
    - is not locked
    - is not deleted/removed
    - is not already flaired
    '''
    return not submission.locked and \
        submission.link_flair_text is None and \
        submission.author is not None and\
        submission.banned_by is None


@retry
def rejectIfInvalid(submission, roundNumber, subredditName):
    '''Lock and comment on a new round if it is titled incorrectly'''

    correctTitlePattern = re.compile(r"^\[Round {}\]".format(roundNumber), re.I)
    roundTitle = submission.title

    if not correctTitlePattern.match(roundTitle):
        Logger.info("Rejecting submission with incorrect title", {
            "title": roundTitle,
            "author": submission.author.name,
            "expectedRoundNumber": roundNumber,
        })

        titleRemainder = TITLE_CORRECTION_PATTERN.sub("", roundTitle)
        correctTitle = "[Round {}] {}".format(roundNumber, titleRemainder)

        utils.commentReply(submission,
            REJECTION_COMMENT,
            subredditName,
            sticky = True,
            correctTitle = correctTitle)

        utils.removeEntity(submission, lock = True)
        return False

    return True


@retry
def checkDeleted(submission):
    '''Check if the current round has been deleted or removed
    Return to listening for rounds if it has'''

    if submission.author is None or submission.banned_by is not None:
        Logger.info("Round deleted, going back to listening for rounds")

        utils.selectFlair(submission, None)
        return True

    return False


@retry
def checkAbandoned(submission, subreddit, currentHost):
    '''Check if the current round has been flaired abandoned or terminated,
    or manually flaired over.'''

    if submission.link_flair_text in ABANDONED_FLAIRS:
        Logger.info("Round abandoned, cleaning up")

        utils.removeContributor(subreddit, currentHost)
        if submission.link_flair_text == OVER_FLAIR:
            return "solved"
        else:
            return "abandoned"

    return None
