from praw.exceptions import APIException

from ..const import CORRECT_PATTERN, COMMENT_URL, ROUND_OVER_STICKY, \
    ROUND_OVER_STICKY_NO_ANSWER, ROUND_OVER_STICKY_WITH_COORDS, UNSUPPORTED_SPOILER_MD_PREFIX, \
    UNSUPPORTED_SPOILER_MD_SUFFIX, MAPS_URL

from ..models import Logger

from ..utils.MarkdownUtils import escapeChars
from ..utils.Retry import retry
from ..utils.Coordinates import getCoords

from . import Post
from . import utils

@retry
def isCurrentRoundCorrect(comment, parent, botName, mods, correctBlacklist):
    '''The following criteria apply for the current round:
        * Passes all criteria in `isPotentialPlusCorrect` and `isPotentiallyEligibleWinner`
    '''

    if not isPotentialPlusCorrect(comment, botName, mods):
        return False

    # No additional reddit requests above this point
    return isEligibleWinner(parent, botName, correctBlacklist)

@retry
def isPastRoundCorrect(comment, parent, botName, mods, api, correctBlacklist, refreshRoundData = False):
    '''The following criteria apply for past rounds:
        * Passes all criteria in `isPotentialPlusCorrect` and `isPotentiallyEligibleWinner`
        * Receiver is not the host (according to the API)
        * The receiving comment is not already the winning comment (according to the API)
        * The +corrector must be the OP, not the host according to the API
            * This ensures that the same person can't verify with their alt
    '''

    if not isPotentialPlusCorrect(comment, botName, mods):
        return False

    roundData = api.getRoundById(comment.submission.id, refresh = refreshRoundData)
    if roundData is None:
        # Not on a round - should never happen
        Logger.warn('Unable to find a round for submission', {
            "submissionId": comment.submission.id,
        })
        return False

    if "winningCommentId" in roundData and parent.id == roundData["winningCommentId"]:
        return False

    # No additional reddit requests above this point
    if parent.author.name == roundData["hostName"]:
        return False

    return isEligibleWinner(parent, botName, correctBlacklist)

@retry
def isPotentialPlusCorrect(comment, botName, mods):
    '''The following checks are common to +corrects on the current round and past rounds:
        * The +correct cannot be a top level comment
        * The +correct must contain the string "+correct"
        * The +correct must not have been deleted/removed
        * The +correct must have been posted by the OP or a mod, and not the bot

        NOTE: None of the checks here should fire any requests to Reddit
    '''

    if comment.is_root:
        return False

    if not CORRECT_PATTERN.search(comment.body):
        return False

    if utils.isDeletedOrRemoved(comment):
        return False

    if comment.author.name.lower() == botName:
        return False

    if not (comment.is_submitter or comment.author.name in mods):
        return False

    # Note: This doesn't necessarily guarantee the comment is a valid +correct
    # Additional checks are needed depending on whether it's on the current round or not
    return True

@retry
def isEligibleWinner(parentComment, botName, correctBlacklist):
    '''These checks are also common to current and past rounds,
    but they require an additional Reddit request to fetch the parent comment's info:
        * The comment must not have been deleted/removed
        * Nobody can +correct the bot, the OP of the round, or other blacklisted users
    '''

    if utils.isDeletedOrRemoved(parentComment):
        return False

    if parentComment.author.name.lower() in correctBlacklist:
        return False

    if parentComment.author.name.lower() == botName:
        return False

    if parentComment.is_submitter:
        return False

    return True

@retry
def postSticky(winningComment, subredditName, existingSticky):
    roundSubmission = winningComment.submission
    roundWinner = winningComment.author.name
    commentLink = COMMENT_URL.format(postId = roundSubmission.id, commentId = winningComment.id)
    roundAnswer = winningComment.body

    answerParts = [part.strip() for part in roundAnswer.split('\n')]
    prefixSanitized = [UNSUPPORTED_SPOILER_MD_PREFIX.sub('', part) for part in answerParts]
    suffixSanitized = [UNSUPPORTED_SPOILER_MD_SUFFIX.sub('', part) for part in prefixSanitized]
    spoileredAnswer = ">!" + "  \n".join([part for part in suffixSanitized if part != ""]) + "!<"

    hasSticky = existingSticky is not None

    coords = getCoords(roundAnswer)
    mapsURL = ''
    if coords:
        lat, lon = coords
        coordsStr = f'{lat},{lon}'
        mapsURL = MAPS_URL.format(coords=coordsStr)

    try:
        if not coords:
            utils.commentReply(roundSubmission,
                ROUND_OVER_STICKY,
                subredditName,
                sticky = not hasSticky,
                winnerName = escapeChars(roundWinner),
                spoileredAnswer = spoileredAnswer,
                commentLink = commentLink,
                throwOnPayloadTooLarge = True)
        else:
            utils.commentReply(roundSubmission,
                ROUND_OVER_STICKY_WITH_COORDS,
                subredditName,
                sticky = not hasSticky,
                winnerName = escapeChars(roundWinner),
                spoileredAnswer = spoileredAnswer,
                mapsLink = mapsURL,
                commentLink = commentLink,
                throwOnPayloadTooLarge = True)

    except APIException as e:
        if e.error_type == "TOO_LONG":
            utils.commentReply(roundSubmission,
                ROUND_OVER_STICKY_NO_ANSWER,
                subredditName,
                sticky = not hasSticky,
                winnerName = escapeChars(roundWinner),
                spoileredAnswer = spoileredAnswer,
                commentLink = commentLink)

@retry
def getStickyComment(submission):
    submission.comments.replace_more(limit = 0)
    for comment in submission.comments.list():
        if comment.stickied:
            return comment
    return None
