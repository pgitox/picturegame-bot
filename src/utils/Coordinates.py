from ..const import DECIMAL_OR_DMS, EVERYTHING_ELSE
from geopy.point import Point
import re
import warnings

def getCoords(text):
    '''
    Returns a lat,long float tuple in decimal format of coordinates in text if found
    '''
    match = re.search(DECIMAL_OR_DMS, text)
    if not match:
        match = re.search(EVERYTHING_ELSE, text)
    try:
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            coord = Point(match[0]) if match else Point(text)
    except ValueError:
        return
    return coord.latitude, coord.longitude
