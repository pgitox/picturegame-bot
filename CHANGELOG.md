# 1.7.5

## Bug Fixes
* Properly clear flairs when round correction results in zero wins

# 1.7.4

## Improvements
* Update wording

# 1.7.3

## Improvements
* Logging improvements

# 1.7.2

## Bug Fixes
* Fix users not being de-approved if we get a volunteer after a round correction

## Improvements
* Send win time to Discord Bot for round corrections
* Send thumbnail url to API for new rounds

# 1.7.1

## Bug Fixes
* Fix crash for first-time winners

# 1.7.0

## New Features
* Update user flairs to use New Reddit's flair templates

# 1.6.7

## Improvements
* Update criteria for archiving "approved submitter" modmail as Reddit changed their wording

# 1.6.6

## Bug Fixes
* Avoid hanging if modmail server is unresponsive

# 1.6.5

## New Features
* Automatically reply to and archive "request to join" modmails
* Wording improvements

# 1.6.4

## Improvements
* Wording improvements

# 1.6.2

## Improvements
* Improve logging in crash situations

# 1.6.0

## New Features
* Add ability to +correct past rounds

# 1.5.1

# Bug Fixes
* Retry API requests if they fail
* Crash fixes

# 1.5.0

## New Features
* Send message to Discord Bot for new Modmail messages
* Send round title and +correct time to the API

## Bug Fixes
* Crash fixes

# 1.4.8

## Improvements
* Update wording of some of the bot's messaging

# 1.4.6

## Improvements
* Send round number and comment body when informing Discord Bot of new comments

# 1.4.4

## New Features
* Send message to the Discord Bot for new comments and new self posts

## Bug Fixes
* Fix crash when logging errors

## Improvements
* Logging improvements

# 1.4.3

## Bug Fixes
* Avoid hanging if the answer is too long to fit in the round-over sticky

# 1.4.2
Coincides with API v1.1.0

## Improvements
* Send authentication credentials with API requests

# 1.4.1

## Bug Fixes
* Fix various crashes during initialization

# 1.4.0

## Improvements
* Major overhaul of the internal structure of the codebase
    * Improves stability and performance
    * Improves ease of adding new features

# 1.3.0
Coincides with API v1.0.0

## Improvements
* Start using the PictureGame API for state initialization and leaderboard handling

# 1.2.1

## Features
* Send winning streak and dry streak information to the Discord bot

## Improvements
* Avoid italicizing names containing underscores

# 1.2.0
Coincides with Discord Bot v1.3.0

## Features
* Send messages via UDP to the Discord bot at start and end of rounds

## Improvements
* Config and logging improvements

# 1.1.0

## Bug Fixes
* Prevent hanging if Reddit returns a 400 error

## Improvements
* Improve stability in large and deeply nested threads
* Improve spoiler tagging in the round-over sticky
* Improve stability of the leaderboard page parser
* Performance improvements
* Logging improvements

# 1.0.0

Initial release

## Features
* All functionality from the old PictureGame bot
* Verification of round number in the title on new rounds
* Responding to new rounds with a standard message (previously done by AutoModerator)
* Post a sticky at the end of the round containing details on the winner and winning comment
* Read the leaderboard from the wiki page, to allow for on-the-fly manual edits
* Prevent +correcting the bot
* Automatically archive the modmail when adding an approved submitter
